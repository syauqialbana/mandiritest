//
//  APIManager.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import Foundation

class APIManager {
    
    static let shared = { APIManager() }()
    
    lazy var baseURL: String = {
        return "https://newsapi.org/v2/"
    }()
    
    var APIKey = "613defc6bf4a42c889f5c9fec68a95a0"
    
}
