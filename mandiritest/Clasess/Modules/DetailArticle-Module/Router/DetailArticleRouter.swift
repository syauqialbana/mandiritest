//
//  DetailArticleRouter.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation
import UIKit

typealias DetailArticleEntryPoint = DetailArticleViewControllerProtocol & DetailArticleViewController

// MARK: Router Input (Presenter -> Router)
protocol DetailArticleRouterProtocol {
    var entryPoint: DetailArticleEntryPoint? { get }
    
    static func start() -> DetailArticleRouterProtocol
}

class DetailArticleRouter: DetailArticleRouterProtocol {
    var entryPoint: DetailArticleEntryPoint?
    
    static func start() -> DetailArticleRouterProtocol {
        let router = DetailArticleRouter()
        
        // MARK: Assign VIP
        var view: DetailArticleViewControllerProtocol = DetailArticleViewController()
        var presenter: DetailArticlePresenterProtocol = DetailArticlePresenter()
        var iteractor: DetailArticleInteractorProtocol = DetailArticleInteractor()
        
        view.presenter = presenter
        iteractor.presenter = presenter
        
        presenter.router = router
        presenter.view = view
        presenter.interactor = iteractor
        
        router.entryPoint = view as? DetailArticleEntryPoint
        
        return router
    }
}
