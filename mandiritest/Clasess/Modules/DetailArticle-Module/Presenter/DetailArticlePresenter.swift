//
//  DetailArticlePresenter.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation

// MARK: View Input (View -> Presenter)
protocol DetailArticlePresenterProtocol {
    
    var view: DetailArticleViewControllerProtocol? { get set }
    var interactor: DetailArticleInteractorProtocol? { get set }
    var router: DetailArticleRouterProtocol? { get set }
}

class DetailArticlePresenter: DetailArticlePresenterProtocol {
    // MARK: Properties
    var view: DetailArticleViewControllerProtocol?
    var interactor: DetailArticleInteractorProtocol?
    var router: DetailArticleRouterProtocol?
}
