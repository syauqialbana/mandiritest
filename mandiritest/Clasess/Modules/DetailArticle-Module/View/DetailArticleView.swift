//
//  DetailArticleViewController.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import SnapKit
import UIKit
import WebKit

// MARK: View Output (Presenter -> View)
protocol DetailArticleViewControllerProtocol {
    var presenter: DetailArticlePresenterProtocol? { get set }
}

class DetailArticleViewController: UIViewController, DetailArticleViewControllerProtocol {
    var presenter: DetailArticlePresenterProtocol?
    var detailArticle: Article? = nil
    
    private let backImageView: UIImageView = {
        let image = UIImage(named: "ic_back")
        var imageView = UIImageView()
        imageView.image = image
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        var label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.text = "Detail Article"
        label.textColor = .white
        label.textAlignment = .right
        return label
    }()
    
    private let webView: WKWebView = {
        var web = WKWebView()
        return web
    }()
    
    private let loadingView: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView()
        loading.color = UIColor.blue
        return loading
    }()
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadWebview()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}

extension DetailArticleViewController {
    private func setupView() {
        view.backgroundColor = UIColor(named: "2469a5")
        
        view.addSubview(backImageView)
        backImageView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(8)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.height.equalTo(16)
            make.width.equalTo(24)
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapBack))
        backImageView.isUserInteractionEnabled = true
        backImageView.addGestureRecognizer(tapGestureRecognizer)
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.bottom.equalTo(backImageView.snp.bottom)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
        }
        
        view.addSubview(webView)
        webView.navigationDelegate = self
        webView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(16)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
        }
        
        view.addSubview(loadingView)
        loadingView.snp.makeConstraints { make in
            make.centerY.equalTo(view.snp.centerY)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
    
    @objc func onTapBack(){
        navigationController?.popViewController(animated: true)
    }
}

extension DetailArticleViewController: WKNavigationDelegate {
    func loadWebview() {
        guard let detailArticle = detailArticle, let urlString = detailArticle.url, let url = URL(string: urlString) else {
            return
        }
        loadingView.startAnimating()
        webView.load(URLRequest(url: url))
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingView.stopAnimating()
    }
}
