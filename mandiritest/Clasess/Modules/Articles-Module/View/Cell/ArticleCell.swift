//
//  ArticleCell.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import UIKit
import Kingfisher

public class ArticleCell: UITableViewCell {
    
    internal lazy var containerView = UIView()
    internal lazy var imgView = UIImageView()
    internal lazy var titleLabel = UILabel()
    internal lazy var descLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellView()
        layoutSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.cornerRadius = 8
    }
    
    private func setupCellView() {
        contentView.addSubview(containerView)
        contentView.backgroundColor = UIColor(named: "2469a5")
        containerView.backgroundColor = .white
        containerView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(contentView.snp.top).offset(8)
            make.leading.equalTo(contentView.snp.leading).offset(16)
            make.trailing.equalTo(contentView.snp.trailing).offset(-16)
            make.bottom.equalTo(contentView.snp.bottom).offset(-8)
        }
        
        let image = UIImage(named: "img_default")
        imgView.image = image
        imgView.clipsToBounds = true
        imgView.layer.cornerRadius = 8
        imgView.contentMode = .scaleAspectFill
        containerView.addSubview(imgView)
        imgView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(containerView.snp.top).offset(16)
            make.leading.equalTo(containerView.snp.leading).offset(16)
            make.trailing.equalTo(containerView.snp.trailing).offset(-16)
            make.height.equalTo(imgView.snp.width).multipliedBy(0.3)
        }
        
        containerView.addSubview(titleLabel)
        titleLabel.textColor = .black
        titleLabel.textAlignment = .left
        titleLabel.font = .boldSystemFont(ofSize: 16)
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(imgView.snp.bottom).offset(8)
            make.leading.equalTo(containerView.snp.leading).offset(16)
            make.trailing.equalTo(containerView.snp.trailing).offset(-16)
        }
        
        containerView.addSubview(descLabel)
        descLabel.textColor = .gray
        descLabel.textAlignment = .left
        descLabel.numberOfLines = 0
        descLabel.font = .systemFont(ofSize: 12)
        descLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.bottom.equalTo(containerView.snp.bottom).offset(-16)
            make.leading.equalTo(containerView.snp.leading).offset(16)
            make.trailing.equalTo(containerView.snp.trailing).offset(-16)
        }
    }
    
    internal func bindData(title: String, desc: String, urlImage: String) {
        titleLabel.text = title
        descLabel.text = desc
        
        let image = UIImage(named: "img_default")
        imgView.kf.indicatorType = .activity
        imgView.kf.setImage(with: URL(string: urlImage), placeholder: image)
    }
}
