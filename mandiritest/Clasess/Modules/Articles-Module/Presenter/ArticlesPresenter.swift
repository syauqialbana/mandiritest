//
//  ArticlesPresenter.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation

// MARK: View Input (View -> Presenter)
protocol ArticlesPresenterProtocol {
    
    var view: ArticlesViewControllerProtocol? { get set }
    var interactor: ArticlesInteractorProtocol? { get set }
    var router: ArticlesRouterProtocol? { get set }
    
    func fetchArticles(source: String, keyword: String, page: Int)
    
    func interactorDidFetchArticlesSuccess(with result: [Article])
    
    func interactorDidFetchArticlesFail(with result: String)
    
    func didSelectArticle(article: Article)
}

class ArticlesPresenter: ArticlesPresenterProtocol {
    // MARK: Properties
    var view: ArticlesViewControllerProtocol?
    var interactor: ArticlesInteractorProtocol?
    var router: ArticlesRouterProtocol?
    
    func fetchArticles(source: String, keyword: String, page: Int) {
        interactor?.getArticles(source: source, keyword: keyword, page: page)
    }
    
    func interactorDidFetchArticlesSuccess(with result: [Article]) {
        view?.update(with: result)
    }
    func interactorDidFetchArticlesFail(with result: String) {
        view?.update(with: result)
    }
    
    func didSelectArticle(article: Article) {
        guard let view = view else {
            return
        }
        router?.pushToDetailArticle(on: view, with: article)
    }
}
