//
//  ArticlesEntity.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import Foundation
import SwiftyJSON

struct Article {

    let source: Source?
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?

    init(_ json: JSON) {
        source = Source(json["source"])
        author = json["author"].string
        title = json["title"].string
        description = json["description"].string
        url = json["url"].string
        urlToImage = json["urlToImage"].string
        publishedAt = json["publishedAt"].string
        content = json["content"].string
    }

    struct Source {

        let id: String?
        let name: String?

        init(_ json: JSON) {
            id = json["id"].string
            name = json["name"].string
        }

    }
}
