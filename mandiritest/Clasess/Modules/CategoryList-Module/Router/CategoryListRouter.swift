//
//  CategoryListRouter.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation
import UIKit

typealias CategoryListEntryPoint = CategoryListViewControllerProtocol & CategoryListViewController

// MARK: Router Input (Presenter -> Router)
protocol CategoryListRouterProtocol {
    var entryPoint: CategoryListEntryPoint? { get }
    
    static func start() -> CategoryListRouterProtocol
    
    func pushToArticles(on view: CategoryListViewControllerProtocol, with source: Source)
}

class CategoryListRouter: CategoryListRouterProtocol {
    var entryPoint: CategoryListEntryPoint?
    
    static func createModule() -> UINavigationController {
        let router = CategoryListRouter()
        
        let view = CategoryListViewController()
        var presenter: CategoryListPresenterProtocol = CategoryListPresenter()
        var iteractor: CategoryListInteractorProtocol = CategoryListInteractor()
        
        view.presenter = presenter
        iteractor.presenter = presenter
        
        presenter.router = router
        presenter.view = view
        presenter.interactor = iteractor
        
        let navigationController = UINavigationController(rootViewController: view)
        return navigationController
    }
    
    static func start() -> CategoryListRouterProtocol {
        let router = CategoryListRouter()
        
        // MARK: Assign VIP
        var view: CategoryListViewControllerProtocol = CategoryListViewController()
        var presenter: CategoryListPresenterProtocol = CategoryListPresenter()
        var iteractor: CategoryListInteractorProtocol = CategoryListInteractor()
        
        view.presenter = presenter
        iteractor.presenter = presenter
        
        presenter.router = router
        presenter.view = view
        presenter.interactor = iteractor
        
        router.entryPoint = view as? CategoryListEntryPoint
        
        return router
    }
    
    func pushToArticles(on view: CategoryListViewControllerProtocol, with source: Source) {
        let articlesVC = ArticlesRouter.start()
        guard let initialVC = articlesVC.entryPoint else {
            return
        }
            
        let viewController = view as! CategoryListViewController
        initialVC.source = source
        viewController.navigationController?.pushViewController(initialVC, animated: true)
    }
}
