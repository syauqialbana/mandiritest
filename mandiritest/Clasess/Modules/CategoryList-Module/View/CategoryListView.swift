//
//  CategoryListViewController.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import SnapKit
import UIKit

// MARK: View Output (Presenter -> View)
protocol CategoryListViewControllerProtocol {
    var presenter: CategoryListPresenterProtocol? { get set }
    
    func update(with categoryList: [Category])
    func update(with error: String)
    func update(with sources: [Source])
}

class CategoryListViewController: UIViewController, CategoryListViewControllerProtocol {
    var presenter: CategoryListPresenterProtocol?
    var categoryList: [Category] = []
    var sources: [Source] = []
    var searchSources: [Source] = []
    var categorySelectedIndex: Int = 0
    var isSearch: Bool = false
    
    private let logoImageView: UIImageView = {
        let image = UIImage(named: "img_logo")
        var imageView = UIImageView()
        imageView.image = image
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        var label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.text = "NEWS"
        label.textAlignment = .right
        return label
    }()
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        
        var collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor(named: "f2f2f2")
        collection.showsHorizontalScrollIndicator = false
        collection.register(CategoryListCell.self, forCellWithReuseIdentifier: "cell")
        collection.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        
        return collection
    }()
    
    private let searchTextField: UITextField = {
        let search = UITextField()
        search.borderStyle = .roundedRect
        return search
    }()
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = UIColor(named: "f2f2f2")
        table.isHidden = true
        table.register(SourceCell.self, forCellReuseIdentifier: "cell")
        table.separatorStyle = .none
        return table
    }()
    
    private let loadingView: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView()
        loading.color = UIColor.blue
        return loading
    }()
    
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadCategoryList()
    }
}

extension CategoryListViewController {
    
    private func setupView() {
        view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(8)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.height.equalTo(40)
            make.width.equalTo(120)
        }
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.bottom.equalTo(logoImageView.snp.bottom)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
        }
        
        view.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(logoImageView.snp.bottom).offset(16)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.height.equalTo(56)
        }
        
        view.addSubview(searchTextField)
        searchTextField.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(collectionView.snp.bottom).offset(8)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
        }
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: searchTextField.frame.height))
        btn.backgroundColor = UIColor.clear
        btn.setImage(UIImage(named: "ic_search"), for: .normal)
        btn.setTitle("  ", for: .normal)
        searchTextField.rightView = btn
        searchTextField.rightViewMode = .always
        searchTextField.placeholder = "Search Source"
        searchTextField.addTarget(self, action: #selector(onSearch), for: .editingChanged)
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(searchTextField.snp.bottom).offset(8)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
        }
        
        view.addSubview(loadingView)
        loadingView.snp.makeConstraints { make in
            make.centerY.equalTo(view.snp.centerY)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
    
    @objc func onSearch() {
        guard let search = searchTextField.text else {
            isSearch = false
            tableView.reloadData()
            return
        }
        isSearch = true
        searchSources = sources.filter({
            ($0.name ?? "").lowercased().contains(search.lowercased())
        })
        tableView.reloadData()
    }
    
    internal func loadCategoryList() {
        loadingView.startAnimating()
        presenter?.fetchCategoryList()
    }
    
    internal func loadSources() {
        loadingView.startAnimating()
        let category = categoryList[categorySelectedIndex]
        presenter?.fetchSources(category: category.title)
    }
}

extension CategoryListViewController {
    // MARK: - CategoryListViewControllerProtocol
    func update(with categoryList: [Category]) {
        loadingView.stopAnimating()
        self.categoryList = categoryList
        loadSources()
        collectionView.reloadData()
        collectionView.isHidden = false
    }
    
    func update(with error: String) {
        print(error)
        loadingView.stopAnimating()
    }
    
    func update(with sources: [Source]) {
        loadingView.stopAnimating()
        self.sources = sources
        tableView.reloadData()
        tableView.isHidden = false
    }
    
}

// MARK: - UICollectionView
extension CategoryListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryListCell
        
        let data = categoryList[indexPath.row]
        cell.updateTitle(title: data.title.capitalized)
        
        let isActive = categorySelectedIndex == indexPath.row
        cell.setStatus(isActive: isActive)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categorySelectedIndex = indexPath.row
        collectionView.reloadData()
        loadSources()
    }
}

extension CategoryListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return searchSources.count
        }
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SourceCell
        cell.selectionStyle = .none
        var data = sources[indexPath.row]
        if isSearch {
            data = searchSources[indexPath.row]
        }
        cell.bindData(title: data.name ?? "" , desc: data.description ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = sources[indexPath.row]
        presenter?.didSelectSource(source: data)
    }
}
